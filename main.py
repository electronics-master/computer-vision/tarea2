#! /usr/bin/env python3

import argparse
import cv2 as cv
import numpy as np
import yaml

from sklearn.cluster import MeanShift, KMeans

THRESHOLD_VALUE = 0  # This value isn't used since Otsu is used for watersheding
THRESHOLD_MAXVAL = 255

DEFAULT_MEAN_SHIFT_DATA = {
    "scaled_down_width": 32,
    "scaled_down_height": 24,
    "bandwidth": 24,
    "max_iters": 50,
}

DEFAULT_KMEANS_DATA = {
    "scaled_down_width": 64,
    "scaled_down_height": 48,
    "n_clusters": 24,
}

DEFAULT_WATERSHED_DATA = {
    "opening_kernel_size": 3,
}

OVERLAY_MASK_TRANSPARENCY = 1
OVERLAY_FRAME_TRANSPARENCY = (1 - OVERLAY_MASK_TRANSPARENCY)

NUM_COLORS = 512

VIDEO_DEVICE = 0


def mean_shift_segment(image, mean_shift, scaled_down_size):
    """Segments an image using a mean-shift

    Args:
        image (np.array): Image to perform the mean shift segmentation over.
        mean_shift (sklearn.cluster.MeanShift): SKLearn mean shift objet to use for the segmentation.
        scaled_down_size (tuple (int, int)): Size to perform the segmentation over

    Returns:
        np.array: Labeled mask with the same size as the input image.
    """
    scaled_down_image = cv.resize(image, scaled_down_size, cv.INTER_CUBIC)

    reshaped_image = np.reshape(
        scaled_down_image, [
            scaled_down_image.shape[0] * scaled_down_image.shape[1], scaled_down_image.shape[2]])

    clustering = mean_shift.fit(reshaped_image)

    labels = clustering.labels_

    reshaped_labels = np.reshape(labels,
                                 [scaled_down_image.shape[0],
                                  scaled_down_image.shape[1],
                                     1]).astype("int8")

    return cv.resize(
        reshaped_labels,
        (image.shape[1],
         image.shape[0]),
        interpolation=cv.INTER_NEAREST)


def k_means_segment(image, k_means, scaled_down_size):
    """Segments an image using kmeans

    Args:
        image (np.array): Image to perform the mean shift segmentation over.
        mean_shift (sklearn.cluster.KMeans): SKLearn kmeans object to use for the segmentation.
        scaled_down_size (tuple (int, int)): Size to perform the segmentation over

    Returns:
        [type]: [description]
    """
    scaled_down_image = cv.resize(image, scaled_down_size, cv.INTER_CUBIC)

    reshaped_image = np.reshape(
        scaled_down_image, [
            scaled_down_image.shape[0] * scaled_down_image.shape[1], scaled_down_image.shape[2]])

    clustering = k_means.fit(reshaped_image)

    labels = clustering.labels_

    reshaped_labels = np.reshape(labels,
                                 [scaled_down_image.shape[0],
                                  scaled_down_image.shape[1],
                                     1]).astype("int8")

    return cv.resize(
        reshaped_labels,
        (image.shape[1],
         image.shape[0]),
        interpolation=cv.INTER_NEAREST)


def watershed_segment(image, kernel_side):
    """Segments an image using the watershed algorithm

    Args:
        image (np.array): Image to perform the watershed segmentation over.
        kernel_side (int) : Opening kernel side.

    Returns:
        np.array: Labeled mask with the same size as the input image.
    """
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    ret, thresh = cv.threshold(
        gray, THRESHOLD_VALUE, THRESHOLD_MAXVAL, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    # noise removal
    kernel = np.ones((kernel_side, kernel_side), np.uint8)
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=2)

    # Marker labelling
    ret, markers = cv.connectedComponents(opening)

    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1

    markers = cv.watershed(image, markers)

    return markers


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='This program demonstrates a several image-based segmentation algorithms.')
    parser.add_argument(
        '--ms',
        help='Perform mean-shift segmentation',
        action="store_true")
    parser.add_argument(
        '--ws',
        help='Perform watershed segmentation',
        action="store_true")
    parser.add_argument(
        '--knn',
        help='Perform k-means segmentation',
        action="store_true")
    parser.add_argument(
        '--video_device',
        help='Video device to use for processing',
        type=int,
        default=0)
    args = parser.parse_args()

    if (not args.knn) and (not args.ms) and (not args.ws):
        parser.print_help()
        exit(1)

    k_means_data = {}
    mean_shift_data = {}
    watershed_data = {}

    try:
        with open("config.yaml") as file:
            data = yaml.load(file, Loader=yaml.FullLoader)

            if "KMeans" in data:
                for key in DEFAULT_KMEANS_DATA:
                    if key in data["KMeans"]:
                        k_means_data[key] = data["KMeans"][key]
                    else:
                        k_means_data[key] = DEFAULT_KMEANS_DATA[key]
            else:
                k_means_data = DEFAULT_KMEANS_DATA

            if "Mean-Shift" in data:
                for key in DEFAULT_MEAN_SHIFT_DATA:
                    if key in data["Mean-Shift"]:
                        mean_shift_data[key] = data["Mean-Shift"][key]
                    else:
                        mean_shift_data[key] = DEFAULT_MEAN_SHIFT_DATA[key]
            else:
                mean_shift_data = DEFAULT_MEAN_SHIFT_DATA

            if "Watershed" in data:
                for key in DEFAULT_WATERSHED_DATA:
                    if key in data["Watershed"]:
                        watershed_data[key] = data["Watershed"][key]
                    else:
                        watershed_data[key] = DEFAULT_WATERSHED_DATA[key]
            else:
                watershed_data = DEFAULT_WATERSHED_DATA

    except IOError:
        print("Configuration file not found, using defaults")

        k_means_data = DEFAULT_KMEANS_DATA
        mean_shift_data = DEFAULT_MEAN_SHIFT_DATA
        watershed_data = DEFAULT_WATERSHED_DATA

    cap = cv.VideoCapture(args.video_device)

    mean_shift = MeanShift(
        bandwidth=mean_shift_data["bandwidth"],
        max_iter=mean_shift_data["max_iters"])
    k_means = KMeans(n_clusters=k_means_data["n_clusters"])

    # Initialize a list of colors
    COLORS = np.random.randint(0, 255, size=(NUM_COLORS - 1, 3), dtype="uint8")

    while (True):
        ret, frame = cap.read()

        if ret:
            if args.ms:
                labeled_image = mean_shift_segment(
                    frame,
                    mean_shift,
                    (mean_shift_data["scaled_down_width"],
                     mean_shift_data["scaled_down_height"]))

                mask = COLORS[labeled_image]

                output = ((OVERLAY_FRAME_TRANSPARENCY * frame) +
                          (OVERLAY_MASK_TRANSPARENCY * mask)).astype("uint8")

                cv.imshow("Mean Shift", output)

            if args.ws:
                labeled_image = watershed_segment(frame, watershed_data["opening_kernel_size"])

                mask = COLORS[labeled_image]

                labeled_image = ((OVERLAY_FRAME_TRANSPARENCY * frame) +
                          (OVERLAY_MASK_TRANSPARENCY * mask)).astype("uint8")

                cv.imshow("Watershed", labeled_image)

            if args.knn:
                labeled_image = k_means_segment(
                    frame,
                    k_means,
                    (k_means_data["scaled_down_width"],
                     k_means_data["scaled_down_height"]))

                mask = COLORS[labeled_image]

                output = ((OVERLAY_FRAME_TRANSPARENCY * frame) +
                          (OVERLAY_MASK_TRANSPARENCY * mask)).astype("uint8")

                cv.imshow("K Means", output)

            if cv.waitKey(1) & 0xFF == ord('q'):
                break

    cap.release()
    cv.destroyAllWindows()
